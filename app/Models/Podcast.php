<?php

namespace App\Models;

use App\Traits\Commentable;
use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Model;

class Podcast extends Model
{
    use Commentable,Taggable;

}
