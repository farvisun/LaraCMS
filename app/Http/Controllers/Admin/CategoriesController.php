<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\Compound;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::get();
//        $category = Category::find(1);
//        $post = $category->posts;
        return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();

        return view('admin.category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $newCategory = Category::create([
            'title'     => $request->input('categoryTitle'),
            'parent_id' => intval($request->input('categoryParent')) > 0 ? $request->input('categoryParent') : null
        ]);

        if ($newCategory && $newCategory instanceof Category) {
            return redirect()->route('admin.categories')->with('status', 'دسته بندی جدید با موفقیت ایجاد شد!');
        }
    }

    public function delete(Request $request, $category_id)
    {
        Category::destroy($category_id);

        return back();
    }

    public function edit(Request $request, $category_id)
    {
        $item       = Category::find($category_id);
        $categories = Category::IdTitles();
        unset($categories[$category_id]);

        return view('admin.category.edit', compact('item', 'categories'));
    }

    public function update(Request $request, $category_id)
    {
        $category = Category::find($category_id);
        $category->update([
            'title'     => $request->input('categoryTitle'),
            'parent_id' => intval($request->input('categoryParent')) > 0 ? $request->input('categoryParent') : null
        ]);
        if (is_a($category, Category::class)) {
            return redirect()->route('admin.categories');
        }
    }
}
